# buran-fake-device

Buran module for Fake devices

# Docker Run

1. Build

docker build -t buran-fake-device .

2. Run

docker run -d -it -e UPDATER_PORT={UPDATER_PORT} -e UPDATER_HOST={UPDATER_HOST} buran-fake-device

Example:

docker run -d -it -e UPDATER_PORT=8080 -e UPDATER_HOST=localhost buran-fake-device