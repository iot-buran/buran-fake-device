const UPDATER_HOST = process.env.UPDATER_HOST || '62.44.108.19'
const UPDATER_PORT = process.env.UPDATER_PORT || 8181

let net = require('net');
const express = require('express')
const app = express()
const port = 3000

let devices = {}

let createDevice = (deviceId, version, user) => {
    let client = new net.Socket();
    devices[deviceId] = {version: version, user: user, client: client}

    client.connect(UPDATER_PORT, UPDATER_HOST, function() {
        console.log('Connected');
        client.write(`CONNECT ${deviceId} ${version} ${user}`);
    });
    
    client.on('data', function(data) {
        var min=1; 
        var max=10;  
        var randomTime =Math.floor(Math.random() * (+max - +min)) + +min; 

        console.log('Received: ' + data);
        let stringData = data.toString();
        if (stringData.split(" ")[0] == "UPDATE") {
            setTimeout(() => {
                client.write(`UPDATESUCCESSFUL ${deviceId} ${stringData.split(" ")[4]}`)
                setTimeout(() => {
                    devices[deviceId]["version"] = stringData.split(" ")[1]
                    createDevice(deviceId, devices[deviceId].version, user)
                }, 2000)
            }, randomTime*1000)
        }
    });
    
    setInterval(() => client.write(`TUPTUP ${deviceId}`), 6000)

    client.on('close', function() {
        console.log('Connection closed');
    });      

    
    client.on('error', function(err) {
        console.log(`Connection error closed: ${err}`);
    });      
}

let disconnectDevice = deviceId => {
    devices[deviceId].client.write(`DISCONNTECT ${deviceId}`);
    delete devices[deviceId]
}

createDevice("12313123123", "1.0.3", "54321")
createDevice("123332423123123", "1.0.3", "54321")
createDevice("1233fgdf2423123123", "1.0.3", "54321")
createDevice("12333423123123", "1.0.3", "54321")
createDevice("12333423123123", "1.0.3", "54321")
createDevice("12333223123123", "1.0.3", "54321")
createDevice("12333243123123", "1.0.3", "54321")
createDevice("12333242123123", "1.0.3", "54321")
createDevice("12333242323123", "1.0.3", "54321")
createDevice("1233323123123", "1.0.3", "54321")
createDevice("132423123123", "1.0.3", "54321")
createDevice("1423123123", "1.0.3", "54321")
createDevice("23332423123123", "1.0.3", "54321")
createDevice("12332423123123", "1.0.3", "54321")
createDevice("12333423123123", "1.0.3", "54321")
createDevice("12333242123123", "1.0.3", "54321")

app.get('/create-device/:deviceId', (req, res) => {
    if (devices[req.params.deviceId]) {
        res.send("Device already exists")
        return
    }
    if (!req.query.version || !req.query.user) {
        res.send("Please specify user and version")
        return
    }
    createDevice(req.params.deviceId, req.query.version, req.query.user)
    res.send("OK")
})

app.get('/destroy-device/:deviceId', (req, res) => {
    disconnectDevice(req.params.deviceId)
    res.send("OK")
})

app.listen(port, () => console.log(`Fake Device app listening on port ${port}!`))

module.exports = {
    createDevice, disconnectDevice
}